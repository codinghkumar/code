package practise.logical.string;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringGeneration {

    private static String[] set = {"A", "B", "C"};
    private static Set dictionarySet = new HashSet();

//	private static void StringGenerator(int[] count, int index) {
//		if(count[0] == set.length && count[count.length] == set.length) {
//			return;
//		}
//		if(count[index] == 26) {
//			index--;
//		}
//
//		count[index]++;
//		index++;
//
//		System.out.println();
//	}

    private static void printString(String str, int length) {
        if(str.length() == length) {
            return;
        }
        for(int i=0;i<set.length;i++) {
            if(dictionarySet.contains(str+set[i])) {
                System.out.println((str+set[i]) + " is duplicate");
            }else {
                dictionarySet.add(str+set[i]);
            }
            printString(str+set[i], length);
        }
    }

    public static void main(String args[]) {
        int len = 3;
//		int[] count = new int[len+1];
//		Arrays.fill(count, 0);
//		StringGenerator(count, 0);
        printString("", len);
        System.out.println("Dictionary length " + dictionarySet.size());	// if total characters=3, len=3(3*3*3)+len=2(3*3)+len=1(3), total=39
        System.out.print(Arrays.toString(dictionarySet.toArray()));
    }


}
