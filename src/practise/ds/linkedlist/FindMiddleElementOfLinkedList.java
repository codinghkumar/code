package practise.ds.linkedlist;

import practise.ds.linkedlist.singlylinkedlist.SimpleNode;
import practise.ds.linkedlist.singlylinkedlist.SinglyLinkedListFactory;

public class FindMiddleElementOfLinkedList {

    public static void main(String args[]){

        SinglyLinkedListFactory<Integer> factory = new SinglyLinkedListFactory<>();
        Integer[] dataArr = {1, 2, 3, 4, 5, 6, 7};
        SimpleNode<Integer> startNode = factory.generateLinkedList(dataArr);

        // Find 3rd node from last
        SimpleNode<Integer> slowPointer = startNode, fastPointer = startNode;

        while (fastPointer != null && fastPointer.getNextNode() != null){
            fastPointer = fastPointer.getNextNode().getNextNode();
            slowPointer = slowPointer.getNextNode();
        }

        System.out.println("Result: " + slowPointer.getData());

    }

}
