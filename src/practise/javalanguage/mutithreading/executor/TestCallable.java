package practise.javalanguage.mutithreading.executor;

import java.util.concurrent.Callable;

public class TestCallable implements Callable<String> {

    private String tag = "";

    @Override
    public String call() throws Exception {
        for(int i=0;i<100000;i++){}
        System.out.println(tag);
        return tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
