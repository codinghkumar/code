package practise.ds.tree.binarytree.bst;

import practise.ds.tree.binarytree.Node;

public class BinarySearchTree {

    public Node root = null;

    public void insert(int value){
        root = insertNode(root, value);
    }

    private Node insertNode(Node node, int value){
        if(node == null){
            node = new Node(value, null, null);
            return node;
        }
        if(node.getKey() > value){
            node.setLeftChild(insertNode(node.getLeftChild(), value));
        }else {
            node.setRightChild(insertNode(node.getRightChild(), value));
        }

        return node;
    }

    public Node search(int value){
        return searchNode(root, value);
    }

    private Node searchNode(Node node, int value){
        if(node == null){
            return null;
        }

        if(node.getKey() == value){
            return node;
        }

        if(node.getKey() > value){
            return searchNode(node.getLeftChild(), value);
        }else {
            return searchNode(node.getRightChild(), value);
        }
    }

    /*
    * 1. Node to be deleted is leaf node -  just delete the node
    * 2. Node to be deleted has only one child - replace node by child node
    * 3. Node to be deleted has two children - replace node by inorder successor or inorder predecessor (instead of deletion just replace the content)
    * */
    public Node delete(int value){
        return deleteNode(root, value);
    }

    private Node deleteNode(Node node, int value){
        if(root == null){
            return null;
        }

        if(node.getKey() == value){

        }

        if(node.getKey() > value){
            return deleteNode(node.getLeftChild(), value);
        }else {
            return deleteNode(node.getRightChild(), value);
        }

    }

}
