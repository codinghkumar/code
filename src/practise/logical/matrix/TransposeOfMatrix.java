package practise.logical.matrix;

import java.util.Arrays;

/*
    https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/
*/

public class TransposeOfMatrix {

    public static void main(String args[]){
        int[][] matrix = {
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25},
                {26, 27, 28, 29, 30},
                {31, 32, 33, 34, 35}
        };
//        System.out.println(Arrays.deepToString(matrix));
        for (int[] value : matrix) {
            System.out.println(Arrays.toString(value));
        }

        int temp = 0;
        int length = matrix.length-1;
        for(int i=0;i<matrix.length/2;i++){
            for(int j=i;j<matrix.length-i-1;j++){
                temp = matrix[i][j];
                // Assign value to Top Left
                matrix[i][j] = matrix[length-j][i];
                // Assign value to Bottom Left
                matrix[length-j][i] = matrix[length-i][length-j];
                //  Assign value to Bottom Right
                matrix[length-i][length-j] = matrix[j][length-i];
                //  Assign value to Top Right
                matrix[j][length-i] = temp;

//                System.out.println();
//                for (int[] ints : matrix) {
//                    System.out.println(Arrays.toString(ints));
//                }
            }
//            System.out.println();
//            for (int[] ints : matrix) {
//                System.out.println(Arrays.toString(ints));
//            }
        }

        System.out.println();
        for (int[] ints : matrix) {
            System.out.println(Arrays.toString(ints));
        }

    }

}
