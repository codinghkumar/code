package practise.systemdesign.quizgenerator.constant;

public enum Level {

    EASY,

    MEDIUM,

    HARD;

    public static Level deserialize(String serializedVal){
        for(Level level : Level.values()){
            if(level.name().equals(serializedVal)){
                return level;
            }
        }
        return null;
    }

}
