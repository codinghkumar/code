package practise.javalanguage.mutithreading.producerconsumerusingbq;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Driver {

    private static BlockingQueue<Integer> blockingQueue = new LinkedBlockingQueue<>();

    public static void main(String args[]){
        Thread producer = new Thread(new Producer(blockingQueue));
        Thread consumer = new Thread(new Consumer(blockingQueue));

        producer.setName("Producer");
        producer.start();
        consumer.setName("Consumer");
        consumer.start();

    }

}
