package practise.ds.tree.binarytree;

public class BinaryTreeUtil {

    private final static int COUNT = 10;

    public static void printBinaryTree(Node root){
        printTree(root, 0);
    }

    private static void printTree(Node root, int space){
        if (root == null)
            return;

        space += COUNT;

        printTree(root.getRightChild(), space);

        System.out.print("\n");
        for (int i = COUNT; i < space; i++)
            System.out.print(" ");
        System.out.print(root.getKey() + "\n");

        printTree(root.getLeftChild(), space);
    }

}
