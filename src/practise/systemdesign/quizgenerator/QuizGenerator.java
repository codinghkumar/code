package practise.systemdesign.quizgenerator;

import practise.systemdesign.quizgenerator.constant.Level;
import practise.systemdesign.quizgenerator.constant.Tag;
import practise.systemdesign.quizgenerator.helper.InputGenerator;
import practise.systemdesign.quizgenerator.helper.QuestionPicker;
import practise.systemdesign.quizgenerator.helper.QuizValidator;
import practise.systemdesign.quizgenerator.model.Question;

import java.util.*;

public class QuizGenerator {

    public static void main(String args[]){
        List<String> questionList = InputGenerator.generateRandomQuestions(600);
        System.out.println(questionList);
        Map<String, List<Question>> questionMap = deserializeQuestions(questionList);

        QuestionPicker questionPicker = new QuestionPicker(questionMap);
        Map<Integer, List<Question>> quiz = new HashMap<>();
        int[] tagIncluded = new int[6];
        int quizCount = 0;

        boolean isQuestionBankExhausted = false;

        while (!isQuestionBankExhausted){
            // Pick questions level wise
            Arrays.fill(tagIncluded, 0);
            List<Question> quizQuestions = new ArrayList<>();

            Question easy1Question = questionPicker.pickQuestion(Level.EASY, null);
            if(easy1Question == null){
                isQuestionBankExhausted = true;
            }else{
                tagIncluded[easy1Question.getTag().ordinal()]++;
                quizQuestions.add(easy1Question);
            }
            Question easy2Question = questionPicker.pickQuestion(Level.EASY, null);
            if(easy2Question == null){
                isQuestionBankExhausted = true;
            }else{
                tagIncluded[easy2Question.getTag().ordinal()]++;
                quizQuestions.add(easy2Question);
            }
            Question medium1Question = questionPicker.pickQuestion(Level.MEDIUM, null);
            if(medium1Question == null){
                isQuestionBankExhausted = true;
            }else{
                tagIncluded[medium1Question.getTag().ordinal()]++;
                quizQuestions.add(medium1Question);
            }
            Question medium2Question = questionPicker.pickQuestion(Level.MEDIUM, null);
            if(medium2Question == null){
                isQuestionBankExhausted = true;
            }else{
                tagIncluded[medium2Question.getTag().ordinal()]++;
                quizQuestions.add(medium2Question);
            }
            Question hard1Question = questionPicker.pickQuestion(Level.HARD, null);
            if(hard1Question == null){
                isQuestionBankExhausted = true;
            }else{
                tagIncluded[hard1Question.getTag().ordinal()]++;
                quizQuestions.add(hard1Question);
            }
            Question hard2Question = questionPicker.pickQuestion(Level.HARD, null);
            if(hard2Question == null){
                isQuestionBankExhausted = true;
            }else {
                tagIncluded[hard2Question.getTag().ordinal()]++;
                quizQuestions.add(hard2Question);
            }

            for(int i=0;i<tagIncluded.length;i++){
                if(tagIncluded[i] == 0){
                    Question question = questionPicker.pickQuestion(null, Tag.values()[i]);
                    if(question == null){
                        isQuestionBankExhausted = true;
                    }else {
                        tagIncluded[question.getTag().ordinal()]++;
                        quizQuestions.add(question);
                    }
                }
            }

            for(int i=quizQuestions.size();i<10;i++){
                Question question = questionPicker.pickQuestion(null, null);
                if(question == null){
                    isQuestionBankExhausted = true;
                    break;
                }else {
                    quizQuestions.add(question);
                }
            }

            if(!isQuestionBankExhausted){
                quizCount++;
                quiz.put(quizCount, quizQuestions);
            }

        }

        System.out.println("Total quizes: " + quizCount);

//        printAllQuizes(quiz);
        QuizValidator.isValidQuizSet(quiz);

    }

    public static Map<String, List<Question>> deserializeQuestions(List<String> serializedQuestionsList){
        Map<String, List<Question>> questionMap = new HashMap<>();
        for(String str : serializedQuestionsList){
            Question question = Question.deserialize(str);
            if(!questionMap.containsKey(question.getKey())){
                questionMap.put(question.getKey(), new ArrayList<>());
            }
            questionMap.get(question.getKey()).add(question);
        }
        return questionMap;
    }

    public static void printAllQuizes(Map<Integer, List<Question>> quiz){
        Set<Map.Entry<Integer, List<Question>>> entries = quiz.entrySet();
        for(Map.Entry<Integer, List<Question>> entry : entries){
            System.out.println(entry.getValue());
        }
    }

}
