package practise.systemdesign.quizgenerator.helper;

import practise.systemdesign.quizgenerator.constant.Level;
import practise.systemdesign.quizgenerator.constant.Tag;
import practise.systemdesign.quizgenerator.model.Index;
import practise.systemdesign.quizgenerator.model.Question;

import java.util.*;

public class QuestionPicker {

    private static Map<String, Index> indexMap = new HashMap<>();
    private static Map<String, String> indexToKeyMap = new HashMap<>();

    private Map<String, List<Question>> questionMap;
    private int[][] matrix = new int[Tag.values().length][Level.values().length];

    static {
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[0], new Index(0,0));
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[1], new Index(1,0));
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[2], new Index(2,0));
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[3], new Index(3,0));
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[4], new Index(4,0));
        indexMap.put(Level.values()[0] + "_"+ Tag.values()[5], new Index(5,0));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[0], new Index(0,1));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[1], new Index(1,1));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[2], new Index(2,1));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[3], new Index(3,1));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[4], new Index(4,1));
        indexMap.put(Level.values()[1] + "_"+ Tag.values()[5], new Index(5,1));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[0], new Index(0,2));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[1], new Index(1,2));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[2], new Index(2,2));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[3], new Index(3,2));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[4], new Index(4,2));
        indexMap.put(Level.values()[2] + "_"+ Tag.values()[5], new Index(5,2));

        for(Map.Entry<String, Index> entry : indexMap.entrySet()){
            indexToKeyMap.put(entry.getValue().getKey(), entry.getKey());
        }
    }

    public QuestionPicker(Map<String, List<Question>> questionMap){
        this.questionMap = questionMap;

        Set<Map.Entry<String, List<Question>>> entrySet = questionMap.entrySet();
        for(Map.Entry<String, List<Question>> entry : entrySet){
            for(Question question : entry.getValue()){
                Index index = indexMap.get(question.getKey());
                matrix[index.getRow()][index.getColumn()]++;
            }
        }

        System.out.println(Arrays.deepToString(matrix)
                .replace("],","\n").replace(",","\t| ")
                .replaceAll("[\\[\\]]", " "));
    }

    public Question pickQuestion(Level level, Tag tag){
        Index index = null;
        if(level != null && tag != null){

        }else if(level != null){
            index = getColumnWiseMax(level.ordinal());
        }else if(tag != null){
            index = getRowWiseMax(tag.ordinal());
        }else {
            index = getMatrixMax();
        }

        if(index != null){
            matrix[index.getRow()][index.getColumn()]--;
            return questionMap.get(indexToKeyMap.get(index.getKey())).remove(0);
        }

        return null;
    }

    private Index getRowWiseMax(int row){
        int max = 0;
        int maxIndex = 0;
        for(int i=0;i<Level.values().length;i++){
            if(matrix[row][i] > max){
                max = matrix[row][i];
                maxIndex = i;
            }
        }

        return max > 0 ? new Index(row, maxIndex) : null;
    }

    private Index getColumnWiseMax(int column){
        int max = 0;
        int maxIndex = 0;
        for(int i=0;i<Tag.values().length;i++){
            if(matrix[i][column] > max){
                max = matrix[i][column];
                maxIndex = i;
            }
        }

        return max > 0 ? new Index(maxIndex, column) : null;
    }

    private Index getMatrixMax(){
        int max = 0;
        int maxRow = 0;
        int maxCol = 0;
        for(int i=0;i<Tag.values().length;i++){
            for(int j=0;j<Level.values().length;j++){
                if(matrix[i][j] > max){
                    max = matrix[i][j];
                    maxRow = i;
                    maxCol = j;
                }
            }
        }

        return max > 0 ? new Index(maxRow, maxCol) : null;
    }

}
