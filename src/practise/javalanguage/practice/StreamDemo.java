package practise.javalanguage.practice;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamDemo {

    public static void main(String args[]){
//        int[] a = {1, 2};
//        int[] b = {3, 4};

//        Integer[] a = {1, 2};
//        Integer[] b = {3, 4};
//        a = null;

//        Integer[] c = Stream.of(a, b).flatMap(Stream::of).toArray(Integer[]::new);

        DemoClass[] a = new DemoClass[2];
        a[0] = new DemoClass(1);
        a[1] = new DemoClass(2);

        DemoClass[] b = new DemoClass[2];
        b[0] = new DemoClass(3);
        b[1] = new DemoClass(4);

        DemoClass[] c = Stream.of(a, b).flatMap(Stream::of).toArray(DemoClass[]::new);
        System.out.println(Arrays.toString(c));
    }

}
