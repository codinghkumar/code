package practise.systemdesign.quizgenerator.helper;

import practise.systemdesign.quizgenerator.constant.Level;
import practise.systemdesign.quizgenerator.constant.Tag;
import practise.systemdesign.quizgenerator.model.Question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QuizValidator {

    public static void isValidQuizSet(Map<Integer, List<Question>> quiz){
        Set<Map.Entry<Integer, List<Question>>> entries = quiz.entrySet();
        for(Map.Entry<Integer, List<Question>> entry : entries){
            System.out.println(entry.getKey() + " " + isValidQuiz(entry.getValue()));
        }

        boolean isDuplicate = false;
        Map<String, Integer> questionMap = new HashMap<>();
        for(Map.Entry<Integer, List<Question>> entry : entries){
            for(Question question : entry.getValue()){
                if(questionMap.containsKey(question.getQuestion())){
                    isDuplicate = true;
                }else {
                    questionMap.put(question.getQuestion(), 1);
                }
            }
        }

        if(isDuplicate){
            System.out.println("Duplicate");
        }else{
            System.out.println("Valid Quiz");
        }
    }

    public static boolean isValidQuiz(List<Question> questions){
        int easyCount = 0, mediumCount = 0, hardCount = 0;
        int tag1Count = 0, tag2Count = 0, tag3Count = 0, tag4Count = 0, tag5Count = 0, tag6Count = 0;

        for(Question question : questions){
            if(question.getLevel() == Level.EASY){
                easyCount++;
            }else if(question.getLevel() == Level.MEDIUM){
                mediumCount++;
            }else {
                hardCount++;
            }

            if(question.getTag() == Tag.TAG1){
                tag1Count++;
            } else if(question.getTag() == Tag.TAG2){
                tag2Count++;
            } else if(question.getTag() == Tag.TAG3){
                tag3Count++;
            } else if(question.getTag() == Tag.TAG4){
                tag4Count++;
            } else if(question.getTag() == Tag.TAG5){
                tag5Count++;
            } else if(question.getTag() == Tag.TAG6){
                tag6Count++;
            }
        }

        if(easyCount > 1 && mediumCount > 1 && hardCount >1 && tag1Count > 0 && tag2Count > 0 && tag3Count > 0 && tag4Count > 0 && tag5Count > 0 && tag6Count > 0){
            return true;
        }

        return false;
    }

}
