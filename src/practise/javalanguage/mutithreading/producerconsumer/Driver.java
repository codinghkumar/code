package practise.javalanguage.mutithreading.producerconsumer;

import java.util.ArrayList;
import java.util.List;

public class Driver {

    private static List<Integer> list = new ArrayList<>();
    private static Object lock = new Object();

    public static void main(String args[]){
        Thread producerThread = new Thread(new Producer(list, lock));
        Thread consumerThread = new Thread(new Consumer(list, lock));

        producerThread.setName("Producer");
        producerThread.start();
        consumerThread.setName("Consumer");
        consumerThread.start();
    }

}
