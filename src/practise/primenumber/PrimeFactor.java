package practise.primenumber;

public class PrimeFactor {

    public static void main(String args[]){
        long n = 6;
        long sum = 0;

        while (n%2==0){
            System.out.print(2 + " ");
            n /= 2;
            sum+=2;
        }

        for (int i = 3; i <= Math.sqrt(n); i+= 2){
            while (n%i == 0){
                System.out.print(i + " ");
                sum+=i;
                n /= i;
            }
        }

        if (n > 2){
            System.out.print(n);
            sum+=n;
        }

        System.out.println();
        System.out.println(sum);

    }

}
