package practise.ds.linkedlist.singlylinkedlist;

/*
* This class creates a simple node with
* generic data type &
* pointer to next node
* */
public class SimpleNode<T> {

    private T data;
    private SimpleNode nextNode;

    public SimpleNode(){

    }

    public SimpleNode(T data, SimpleNode nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public SimpleNode getNextNode() {
        return nextNode;
    }

    public void setNextNode(SimpleNode nextNode) {
        this.nextNode = nextNode;
    }

}
