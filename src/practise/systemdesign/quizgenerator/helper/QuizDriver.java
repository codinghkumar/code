package practise.systemdesign.quizgenerator.helper;

import practise.systemdesign.quizgenerator.model.Question;

import java.util.*;

public class QuizDriver {

    public static void main(String args[]){
        List<String> questionList = InputGenerator.generateRandomQuestions(500);
        System.out.println(questionList);
        Map<String, List<Question>> questionMap = deserializeQuestions(questionList);

        Set<Map.Entry<String, List<Question>>> entrySet = questionMap.entrySet();
        for(Map.Entry<String, List<Question>> entry : entrySet){
            System.out.println(entry.getKey() + " " + entry.getValue().size());
        }

    }

    public static Map<String, List<Question>> deserializeQuestions(List<String> serializedQuestionsList){
        Map<String, List<Question>> questionMap = new HashMap<>();
        for(String str : serializedQuestionsList){
            Question question = Question.deserialize(str);
            if(!questionMap.containsKey(question.getKey())){
                questionMap.put(question.getKey(), new ArrayList<>());
            }
            questionMap.get(question.getKey()).add(question);
        }
        return questionMap;
    }

}
