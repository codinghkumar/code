package practise.javalanguage.collections;

import java.util.*;
import java.util.stream.Stream;

/*
* https://www.geeksforgeeks.org/iterate-map-java/
* */
public class MapDemo {

    public static void main(String args[]){
        HashMapDemo hashMapDemo = new HashMapDemo();
        hashMapDemo.execute();
    }

    static class HashMapDemo{

        public void execute(){
            HashMap<String, Integer> map = new HashMap<>();
            map.put("A", 1);
            map.put("B", 2);
            map.put("C", 3);
            map.put("A1", 4);
            map.put("B1", 5);
            map.put("C1", 6);
//        map.put("A", 1);

            // iterating using for loop over keyset
            System.out.println("Iterating over map keyset using simple for loop");
            for(Map.Entry<String, Integer> i : map.entrySet()){
                System.out.println(i.getKey());
            }
            System.out.println();

            // iterating using for loop over values
            System.out.println("Iterating over map values using simple for loop");
            for(Integer i : map.values()){
                System.out.println(i);
            }
            System.out.println();

            // iterating using iterator
            System.out.println("Iteration over map using iterator");
            Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()){
                Map.Entry<String, Integer> entry = iterator.next();
                System.out.println("Key:-" + entry.getKey() + " Value:-" + entry.getValue());
            }
            System.out.println();

            // iteration using foreach loop Java 8
            System.out.println("Iteration over map using foreach loop");
            map.forEach((k, v) -> {System.out.println("Key:-" + k + " Value:-" + v);});
            System.out.println();

            // iterate over map using stream
            Stream<Map.Entry<String, Integer>> entryStream = map.entrySet().stream();
            System.out.println(Arrays.toString(entryStream.toArray()));

            Stream<Map.Entry<String, Integer>> entryStream2 = map.entrySet().stream();
            entryStream2.forEach((entry) -> {System.out.println("Key:-" + entry.getKey() + " Value:-" + entry.getValue());});

            Stream<Map.Entry<String, Integer>> entryStream3 = map.entrySet().stream();
            System.out.println(Arrays.toString(entryStream3.toArray()));

            Stream<Integer> valueStream = map.values().stream();
            System.out.println(Arrays.toString(valueStream.toArray()));

            Map<String, Integer> unmodifiableMap = Collections.unmodifiableMap(map);
            map.put("D", 7);    // you can still add values to existing instance of map
//            unmodifiableMap.put("D1", 8);   // will throw runtime exception Exception in thread "main" java.lang.UnsupportedOperationException

            Map<String, Integer> synchronizedMap = Collections.synchronizedMap(map);    // here map will still be un-synchronized but the object returned by Collections.synchronizedMap will be synchronized
        }

    }

}
