package practise.javalanguage.mutithreading.printnumbers;

public class Driver {

    public static void main(String args[]){
        NumberingRunnable runnable1 = new NumberingRunnable(1);
        NumberingRunnable runnable2 = new NumberingRunnable(2);
        NumberingRunnable runnable3 = new NumberingRunnable(0);

        Thread thread1 = new Thread(runnable1);
        thread1.setName("ThreadOne");
        Thread thread2 = new Thread(runnable2);
        thread2.setName("ThreadTwo");
        Thread thread3 = new Thread(runnable3);
        thread3.setName("ThreadThree");

        thread1.start();
        thread2.start();
        thread3.start();
    }

}
