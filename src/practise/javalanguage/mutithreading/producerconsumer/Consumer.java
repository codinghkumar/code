package practise.javalanguage.mutithreading.producerconsumer;

import java.util.List;

public class Consumer implements Runnable {

    private List<Integer> list;
    private Object lock;

    public Consumer(List<Integer> list, Object lock) {
        this.list = list;
        this.lock = lock;
    }

    @Override
    public void run() {
        try{
            synchronized (lock){
                while (true){
                    System.out.println(Thread.currentThread().getName() + ": list-size:" + list.size());
                    if(list.size() > 0){
                        int val = list.remove(0);
                        System.out.println(Thread.currentThread().getName() + ": " + val);
                        lock.notifyAll();
                    }else {
                        lock.wait();
                    }
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
