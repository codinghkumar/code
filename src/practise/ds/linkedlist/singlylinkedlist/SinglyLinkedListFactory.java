package practise.ds.linkedlist.singlylinkedlist;

public class SinglyLinkedListFactory<T> {

    /*
    * This method generates & returns a singly linked list
    * Params:
    *   array of data
    * Returns:
    *   pointer to first node
    * */
    public SimpleNode generateLinkedList(T[] dataArray){
        SimpleNode<T> startPointer = new SimpleNode<>(null, null);
        SimpleNode currentNode = startPointer;

        for(T i : dataArray){
            SimpleNode<T> node = new SimpleNode<>(i, null);
            currentNode.setNextNode(node);
            currentNode = node;
        }

        return startPointer;
    }

}
