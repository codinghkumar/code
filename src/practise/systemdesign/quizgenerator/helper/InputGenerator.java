package practise.systemdesign.quizgenerator.helper;

import practise.systemdesign.quizgenerator.constant.Level;
import practise.systemdesign.quizgenerator.constant.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InputGenerator {

    private static final String seperator = "|";

    public static List<String> generateRandomQuestions(int numberOfQuestions){
        List<String> questionsList = new ArrayList<>();
        Random random = new Random();

        for(int i=1;i<=numberOfQuestions;i++){
            int randomLevel = random.nextInt(Integer.MAX_VALUE-0);
            Level level = Level.values()[randomLevel % Level.values().length];
//            if(i>300 && level == Level.EASY){
//                level = Level.MEDIUM;
//            }
            int randomTag = random.nextInt(Integer.MAX_VALUE-0);
            Tag tag = Tag.values()[randomTag % Tag.values().length];
//            if (i > 300 && (tag == Tag.TAG1 || tag == Tag.TAG2)) {
//                tag = Tag.TAG4;
//            }

            StringBuilder builder = new StringBuilder();
            builder.append("Q").append(i).append(seperator);
            builder.append(level).append(seperator);
            builder.append(tag);

            questionsList.add(builder.toString());
        }

        return questionsList;
    }

}
