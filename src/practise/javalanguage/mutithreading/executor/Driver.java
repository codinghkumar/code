package practise.javalanguage.mutithreading.executor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/*
* https://www.geeksforgeeks.org/callable-future-java/
* */
public class Driver {

    public static void main(String args[]) throws InterruptedException {
        Callable<String>[] callables = new Callable[10];

        for(int i=0;i<10;i++){
            callables[i] = new TestCallable();
            ((TestCallable) callables[i]).setTag(String.valueOf(i));
        }

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        List<Future<String>> futureList = executorService.invokeAll(Arrays.asList(callables));

        // Future is interface & FutureTask is an implementation,
        // internally executor also created FutureTask for every callable so there is no need to use FutureTask directly
        // FutureTask implements Runnable & Future interface
        // Future provide no way to get callback when task is completed only way is to periodically check, to overcome this problem we can use CompletableFuture
        FutureTask<String> futureTask = new FutureTask<>(new TestCallable());
        executorService.submit(futureTask);

        CompletableFuture<String> completableFuture = new CompletableFuture<>();


    }

}
