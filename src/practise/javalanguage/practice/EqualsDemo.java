package practise.javalanguage.practice;

import java.util.Objects;

public class EqualsDemo {

    private int a;
    private String str;

    public EqualsDemo(int a, String str){
        this.a = a;
        this.str = str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EqualsDemo that = (EqualsDemo) o;
        return a == that.a &&
                Objects.equals(str, that.str);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, str);
    }
}
