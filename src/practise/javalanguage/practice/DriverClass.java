package practise.javalanguage.practice;

public class DriverClass {

    public static void main(String args[]){
        BaseClass baseClass = null;
        baseClass.printStr();

//        BaseClass baseClass1 = new BaseClass(); // would cause ClassCastException
        BaseClass baseClass1 = new ChildClass();  // run successfully
        // So we should always check if baseclass contains reference of child class only then we should cast it
        if(baseClass1 instanceof ChildClass){
            ChildClass childClass = (ChildClass) baseClass1;
        }

    }

}
