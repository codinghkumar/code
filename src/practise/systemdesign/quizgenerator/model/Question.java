package practise.systemdesign.quizgenerator.model;

import practise.systemdesign.quizgenerator.constant.Level;
import practise.systemdesign.quizgenerator.constant.Tag;

public class Question {

    private Level level;
    private String question;
    private Tag tag;

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String serialize(){
        return question + "|" + level + "|" + tag;
    }

    public static Question deserialize(String serializedQuestion){
        Question question = new Question();
        String[] tokens = serializedQuestion.split("\\|");
        question.setQuestion(tokens[0]);
        question.setLevel(Level.deserialize(tokens[1]));
        question.setTag(Tag.deserialize(tokens[2]));
        return question;
    }

    public String getKey(){
        return level + "_" + tag;
    }

    @Override
    public String toString() {
        return "Question{" +
                "level=" + level +
                ", question='" + question + '\'' +
                ", tag=" + tag +
                '}';
    }
}
