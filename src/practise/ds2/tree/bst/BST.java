package practise.ds2.tree.bst;

/*
    https://www.geeksforgeeks.org/binary-search-tree-set-2-delete/
*/

import java.util.LinkedList;
import java.util.Queue;

public class BST {

    private Node root;

    public BST(){
        root = null;
    }

    public class Node{
        int key;
        Node left, right;

        public Node(int key){
            this.key = key;
            left = right = null;
        }
    }

    private void insertNode(int key){
        root = insertNode(root, key);
    }

    private void deleteNode(int key){
        root = deleteNode(root, key);
    }

    private Node insertNode(Node root, int key){
        if(root == null){
            return new Node(key);
        }else if(key < root.key){
            root.left = insertNode(root.left, key);
        }else if(key > root.key){
            root.right = insertNode(root.right, key);
        }

        return root;
    }

    private Node deleteNode(Node root, int key){
        if(root == null){
            return root;
        }
        if(key < root.key){
            root.left = deleteNode(root.left, key);
        }else if(key > root.key){
            root.right = deleteNode(root.right, key);
        }else { // node to be deleted has been found
            if(root.left == null){  // node has only right child
                return root.right;
            }else if(root.right == null){ // node has only left child
                return root.left;
            }else { // node has both child
                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                // can be replaced by inorder successor or inorder predecessor
                root.key = findInorderSuccessor(root);

                // Delete the inorder successor
                root.right = deleteNode(root.right, root.key);
            }
        }
        return root;
    }

    private int findInorderSuccessor(Node root){
        int value = root.key;
        while (root.left != null){
            value = root.left.key;
            root = root.left;
        }
        return value;
    }

    private void levelOrder(){
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        queue.add(null);    // null to separate levels
        while (!queue.isEmpty()){
            Node currentNode = queue.poll();
            if(currentNode == null){
                System.out.println();
                if(!queue.isEmpty()){
                    queue.add(null);
                }
                continue;
            }
            if(currentNode.left != null){
                queue.add(currentNode.left);
            }
            if(currentNode.right != null){
                queue.add(currentNode.right);
            }

            System.out.print(currentNode.key + " ");
        }
    }

    /*
    *   https://www.geeksforgeeks.org/zigzag-tree-traversal/
    **/
    private void printZigZagLevelTraversal(Node root){

    }

    /*
    * https://www.geeksforgeeks.org/print-binary-tree-vertical-order/
    * https://www.geeksforgeeks.org/print-binary-tree-vertical-order-set-2/
    * https://www.geeksforgeeks.org/print-a-binary-tree-in-vertical-order-set-3-using-level-order-traversal/
    * */
    private void printVerticalOrderTraversal(Node root){

    }

    /*
    * https://www.geeksforgeeks.org/print-left-view-binary-tree/
    * */
    private void printLeftView(Node root){

    }

    /*
    * https://www.geeksforgeeks.org/print-nodes-top-view-binary-tree/
    * */
    private void printTopView(Node root){

    }

    /*
    * https://www.geeksforgeeks.org/bottom-view-binary-tree/
    * */
    private void printBottomView(Node root){

    }

    public static void main(String[] args){
        BST bst = new BST();
        bst.insertNode(50);
        bst.insertNode(30);
        bst.insertNode(20);
        bst.insertNode(40);
        bst.insertNode(70);
        bst.insertNode(60);
        bst.insertNode(80);

        bst.levelOrder();

    }

}
