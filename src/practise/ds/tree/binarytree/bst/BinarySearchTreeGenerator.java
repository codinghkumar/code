package practise.ds.tree.binarytree.bst;

import practise.ds.tree.binarytree.BinaryTreeUtil;
import practise.ds.tree.binarytree.Node;

import java.util.Arrays;

public class BinarySearchTreeGenerator {

    /*
    *                                     50
    *                                 /      \
    *                               30       70
    *                              /  \     /  \
    *                            20   40   60  80
    * */

    public static void main(String args[]){
        BinarySearchTree bst = new BinarySearchTree();
        // insertion O(h) & O(n) for skewed tree
        // https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
        bst.insert(50);
        bst.insert(30);
        bst.insert(20);
        bst.insert(40);
        bst.insert(70);
        bst.insert(60);
        bst.insert(80);

        System.out.println(bst);

        BinaryTreeUtil.printBinaryTree(bst.root);

        // search O(h) & O(n) for skewed tree
        Node searchedNode30 = bst.search(30);
        System.out.println(searchedNode30);
        Node searchedNode40 = bst.search(40);
        System.out.println(searchedNode40);
        Node searchedNode70 = bst.search(70);
        System.out.println(searchedNode70);

        // deletion O(h) & O(n) for skewed tree
//        Node deletedNode30 = bst.delete(30);
//        System.out.println(deletedNode30);

//        String url = "https://zillious.com/cart/user//1234";
//        String[] urlArr = url.split("/");
//        System.out.println(Arrays.toString(urlArr));

    }

}
