package practise.primenumber;

import java.util.HashMap;
import java.util.Map;

public class MinimumSumSubarray {

    public static void main(String args[]){
        int[] array = {99, -50, -47,-72, -56, -65, -99, -11, 40, -23, -94, 37, -53, 70, -41, -3, 83, -64, -37, -25};
        int[] array1 = {-31, 81, -4, -4, -16, 86, -10, -81, -44, 4, 4, -22, -11, 2, 40, -92, 11, -44, 15, 56};
        System.out.println(numberOfArrays(array));
    }

    public static int numberOfArrays(int[] numbers){
        int sum = Integer.MAX_VALUE;
        int min = Integer.MAX_VALUE;
        int arrayCount = 0;
        int temp = 0;

        for(int i = 0; i < numbers.length; i++){
            if(sum > 0){
                sum = numbers[i];
            }else{
                sum+=numbers[i];
            }
            min = min < sum ? min : sum;
        }

        Map<Integer, Integer> tempMap = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            temp+=numbers[i];

            if (temp==min){
                arrayCount++;
            }

            if (tempMap.containsKey(temp-min)){
                arrayCount += tempMap.get(temp-min);
            }

            if (tempMap.containsKey(temp)){
                tempMap.put(temp, tempMap.get(temp)+1);
            }else{
                tempMap.put(temp, 1);
            }
        }

        return arrayCount;
    }

}
