package practise.sorting;

public class Utils {

    public static boolean isIntArraySorted(int[] array){
        boolean isSorted = true;
        for(int i=1;i<array.length;i++){
            if(array[i] < array[i-1]){
                isSorted = false;
            }
        }
        return isSorted;
    }

    public static boolean isIntArraySortedInDecOrder(int[] array){
        boolean isSorted = true;
        for(int i=1;i<array.length;i++){
            if(array[i] > array[i-1]){
                isSorted = false;
            }
        }
        return isSorted;
    }

}
