package practise.javalanguage.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArrayListDemo {

    public static void main(String args[]){
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");

//        Integer intVal = 1;
//        list.add(intVal); // can't add Integer
//        DemoClass demoClass = new DemoClass(1);
//        list.add(demoClass); // can't add any other class

        Map<String, String> map = new HashMap<>();
        map.put("1", "val1");
        map.put("2", "val2");

        String[] arr = {"1", "2"};

        System.out.println(list);
        System.out.println(map);
        System.out.println(arr);
    }

}
