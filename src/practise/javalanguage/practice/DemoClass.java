package practise.javalanguage.practice;

public class DemoClass {

    private int val;

    public DemoClass(int intVal){
        val = intVal;
    }

    @Override
    public String toString() {
        return String.valueOf(val);
    }
}
