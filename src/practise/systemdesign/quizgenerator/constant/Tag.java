package practise.systemdesign.quizgenerator.constant;

public enum Tag {

    TAG1,
    TAG2,
    TAG3,
    TAG4,
    TAG5,
    TAG6;

    public static Tag deserialize(String serializedVal){
        for(Tag tag : Tag.values()){
            if(tag.name().equals(serializedVal)){
                return tag;
            }
        }
        return null;
    }

}
