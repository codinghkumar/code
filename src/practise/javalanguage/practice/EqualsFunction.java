package practise.javalanguage.practice;

public class EqualsFunction {

    public static void main(String args[]){
        EqualsDemo equalsDemo1 = new EqualsDemo(1, "a");
        EqualsDemo equalsDemo2 = new EqualsDemo(1, "a");
        EqualsDemo equalsDemo3 = new EqualsDemo(2, "b");

        System.out.println("equalsDemo1 == equalsDemo3 " + (equalsDemo1 == equalsDemo3));
        System.out.println("equalsDemo1 == equalsDemo1 " + (equalsDemo1 == equalsDemo1));
        System.out.println("equalsDemo1 == equalsDemo2 " + (equalsDemo1 == equalsDemo2));
        System.out.println("equalsDemo1.equals(equalsDemo2) " + equalsDemo1.equals(equalsDemo2));   // to get it work we have to override equals() method
        System.out.println("equalsDemo1.equals(equalsDemo3) " + equalsDemo1.equals(equalsDemo3));
    }

}
