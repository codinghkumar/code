package practise.javalanguage.mutithreading.producerconsumer;

import java.util.List;
import java.util.Scanner;

public class Producer implements Runnable {

    private List<Integer> list;
    private Object lock;
    private Scanner sc;

    public Producer(List<Integer> list, Object lock) {
        this.list = list;
        this.lock = lock;
        sc = new Scanner(System.in);
    }

    @Override
    public void run() {
        try{
            synchronized (lock){
                while (true){
                    System.out.println(Thread.currentThread().getName() + ": Please enter the number: ");
                    list.add(sc.nextInt());
                    System.out.println(Thread.currentThread().getName() + ": list-size:" + list.size());
                    lock.wait();
                    lock.notifyAll();
                }
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }

    }

}
