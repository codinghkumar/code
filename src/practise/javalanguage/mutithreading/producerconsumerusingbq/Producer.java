package practise.javalanguage.mutithreading.producerconsumerusingbq;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    private BlockingQueue<Integer> blockingQueue;
    private Scanner sc;

    public Producer(BlockingQueue<Integer> blockingQueue) {
        this.blockingQueue = blockingQueue;
        this.sc = new Scanner(System.in);
    }

    @Override
    public void run() {
        while (true){
            System.out.println(Thread.currentThread().getName() + ": add value: ");
            int val = sc.nextInt();
            blockingQueue.add(val);
        }
    }

}
