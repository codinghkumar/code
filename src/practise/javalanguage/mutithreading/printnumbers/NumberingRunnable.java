package practise.javalanguage.mutithreading.printnumbers;

public class NumberingRunnable implements Runnable {
    private static int counter = 1;
    private int identifier;

    private static Object lock = new Object();

    public NumberingRunnable(int count){
        this.identifier = count;
    }

    @Override
    public void run() {
        while (counter < 99){
            synchronized (lock){
                while (counter%3 != identifier){    // put other thread in wait state until remainder not equal to identifier, kind of infinite loop
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName() + " " + counter++);
                lock.notifyAll();
            }
        }
    }

}
