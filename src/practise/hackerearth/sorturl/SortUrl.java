package practise.hackerearth.sorturl;

import java.util.*;

public class SortUrl {

    private static Map<String, Integer> map = new TreeMap<>();

    public static void main(String args[]){

        ArrayList<String>[] list = new ArrayList[5];

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return map.get(o2).compareTo(map.get(o1));
            }
        };

        Collections.reverseOrder();
    }

}
